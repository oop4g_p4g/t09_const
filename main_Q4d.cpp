////////////////////////////////////////////////////////////////////////
// OOP Tutorial: More on Classes and Instances (question 4c) 
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <iostream>	
using namespace std;


//Question 4c: returning instances
class Score {
public:
    Score();
    Score( int a);
	~Score();
    Score( const Score& s);
    int getAmount() const;
    void setAmount( int a);
	void showAmount() const;
    //...
private:
    int amount_;
};
Score::Score() : amount_(0) {
    cout << "\n Score() called: ";
}
Score::Score( int a) : amount_(a) {
    cout << "\n Score( int a) called: ";
}
Score::Score( const Score& s) : amount_(s.amount_) {
    cout << "\n Score( const Score&) called: ";
}
Score::~Score() {
    cout << "\n ~Score() called: ";
}
int Score::getAmount() const {
    return amount_;
}
void Score::setAmount( int a) {
    amount_ = a;
}
void Score::showAmount() const {
    cout << amount_;
}

class Player
{
public:
	Score getScore() const {
		return sc_;
	}
private:
	Score sc_;
};

int main() {
	//returning values

	Player p;
	p.getScore() = 12;
	p.getScore().showAmount();


	cout << "\n\n";
	system( "pause");
	return 0;
}